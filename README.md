** Beta testers needed! ** to be a part of the beta test, please join the [SPT beta test](https://plus.google.com/u/0/communities/103688613831596380167) Google+ community

SPT
===

SSH Persistent Tunnel (SPT) tries to maintain local ssh tunnels over the frequent change of network and disconnects typical for a mobile device. SPT tries to reconnect each time a new network comes up.

# Features
* supports authentication with passwords or private key. 
* supports Local, Remote and Dynamic (SOCKS over SSH) tunnels.
* maintains a known-hosts database for prevention of MITM attacks.
* strives to be as lightweight as possible to conserve system resources.
* _never stores your password!_ login password or private key passphrase are stored in memory only.
* preferences can be exported/imported to/from sdcard
* Supports [ExternalControl](wiki/ExternalControl) through intents (also [TaskerLocaleControl](wiki/TaskerLocaleControl) )
* Open Source - Don't trust me -- check SPT's security for yourself ...

More info in the [wiki](wiki/Home)

# How to get it
You can either clone the source and build yourself, or get it from the [Google Play Store](https://market.android.com/details?id=org.ayal.SPT)  or The [ Amazon Appstore for Android](http://www.amazon.com/Shai-Ayal-SSH-persitent-tunnels/dp/B00G6POBP8)

While the android market version is not free as in beer, it is very modestly priced.

# License
SPT free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.

See the LICENSE directory for a list of licences used in parts of SPT